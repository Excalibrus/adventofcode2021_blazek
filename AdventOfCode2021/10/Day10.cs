﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2021
{
  public class Day10
  {
    public static void Solve(int part)
    {
      string[] lines = File.ReadAllLines("10/10.txt");
      HashSet<char> openingChars = new HashSet<char> { '(', '[', '{', '<' };
      Dictionary<char, char> opposites = new Dictionary<char, char>
      {
        { '(', ')' },
        { '[', ']' },
        { '{', '}' },
        { '<', '>' }
      };
      Dictionary<char, (int Corrupt, int Incomplete)> charValues = new Dictionary<char, (int Corrupt, int Incomplete)>
      {
        { ')', (3, 1) },
        { ']', (57, 2) },
        { '}', (1197, 3) },
        { '>', (25137, 4) }
      };
      
      long syntaxErrorScore = 0;
      List<long> autocompleteScores = new List<long>();
      
      foreach (string line in lines)
      {
        List<char> nextCharsToClose = new List<char>();
        bool isCorrupted = false;
        
        foreach (char c in line)
        {
          if (openingChars.Contains(c))
          {
            nextCharsToClose.Add(opposites[c]);
          }
          else
          {
            char? nextCharToClose = nextCharsToClose.LastOrDefault();
            if (c == nextCharToClose)
            {
              nextCharsToClose.RemoveAt(nextCharsToClose.Count - 1);
            }
            else
            {
              syntaxErrorScore += charValues[c].Corrupt;
              isCorrupted = true;
              break;
            } 
          }
        }

        if (!isCorrupted)
        {
          long incompleteScore = 0;
          nextCharsToClose.Reverse();
          foreach (char c in nextCharsToClose)
          {
            incompleteScore = incompleteScore * 5 + charValues[c].Incomplete;
          }

          autocompleteScores.Add(incompleteScore);
        }
      }

      Console.WriteLine(part == 1 ? $"Total syntax error score is {syntaxErrorScore}" : $"Middle autocomplete score is {autocompleteScores.OrderBy(x => x).ToList()[autocompleteScores.Count / 2]}");
    }
  }
}