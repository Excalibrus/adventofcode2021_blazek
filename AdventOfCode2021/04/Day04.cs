﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2021
{
  public class Day04
  {
    private class Board
    {
      public BoardField[] Fields { get; set; }
      public bool Won { get; set; }
      public int Index { get; set; }

      public Board(BoardField[] fields, bool won, int index)
      {
        Fields = fields;
        Won = won;
        Index = index;
      }
    }
      
    private class BoardField
    {
      public int RowIndex { get; set; }
      public int ColumnIndex { get; set; }
      public int Value { get; set; }
      public bool Matched { get; set; }
      
      public BoardField(int rowIndex, int columnIndex, int value, bool matched)
      {
        RowIndex = rowIndex;
        ColumnIndex = columnIndex;
        Value = value;
        Matched = matched;
      }
    }
    
    public static void Solve(int part)
    {
      int n = 5;
      List<int> numbers = null;
      List<Board> boards = new List<Board>();
      int rowIndex = -1;
      using StreamReader sr = new StreamReader("04.txt");
      string line;

      List<BoardField> tmp = null;
      while ((line = sr.ReadLine()) != null)
      {
        if (numbers == null)
        {
          numbers = line.Split(",").Select(int.Parse).ToList();
        }
        else if (line.Length == 0)
        {
          tmp = new List<BoardField>();
          rowIndex = 0;
        }
        else if (tmp != null)
        {
          tmp.AddRange(line.Split(" ").Where(x => x.Length > 0).Select((x, i) => new BoardField(rowIndex, i, int.Parse(x), false)));
          rowIndex++;
          if (rowIndex == n)
          {
            boards.Add(new Board(tmp.ToArray(), false, boards.Count));
          }
        }
      }

      if (numbers == null)
      {
        throw new Exception();
      }

      int result = 0;
      (int BoardIndex, int Number) lastlyWonData = (-1, -1);
      foreach (int number in numbers)
      {
        foreach (Board board in boards.Where(x => !x.Won).ToList())
        {
          BoardField field = board.Fields.FirstOrDefault(x => x.Value == number && !x.Matched);
          if (field != null)
          {
            Board relevantBoard = boards.First(x => x.Index == board.Index);
            relevantBoard.Fields.First(x => x.RowIndex == field.RowIndex && x.ColumnIndex == field.ColumnIndex).Matched = true;
            
            for (int i = 0; i < n; i++)
            {
              if (relevantBoard.Fields.Where(x => x.RowIndex == i).All(x => x.Matched) || relevantBoard.Fields.Where(x => x.ColumnIndex == i).All(x => x.Matched))
              {
                relevantBoard.Won = true; 
                result = relevantBoard.Fields.Where(x => !x.Matched).Sum(x => x.Value) * number;
                lastlyWonData = (relevantBoard.Index, number);
              
                if (part == 1 || boards.All(x => x.Won))
                {
                  goto END;
                }
              }
            }
          }
        }
      }

      END:
      if (part == 2)
      {
        result = boards.First(x => x.Index == lastlyWonData.BoardIndex).Fields.Where(x => !x.Matched).Sum(x => x.Value) * lastlyWonData.Number;
      }
      
      Console.WriteLine($"Winning score is: {result}");
    }
  }
}