﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AdventOfCode2021
{
  public class Day03
  {
    public static void Solve(int part)
    {
      (int[] Zeros, int[] Ones) result = (null, null);
      List<string> bits = new List<string>();
      using (StreamReader sr = new StreamReader("03.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          bits.Add(line);
          
          if (result.Zeros == null)
          {
            result = (new int[line.Length], new int[line.Length]);
          }

          for (int i = 0; i < line.Length; i++)
          {
            if (line[i] == '0')
            {
              result.Zeros[i]++;
            }
            else
            {
              result.Ones[i]++;
            }
          }
        }
      }

      if (result.Zeros == null || result.Ones == null)
      {
        throw new Exception();
      }

      if (part == 1)
      {
        int gammaRate = 0;
        int epsilonRate = 0;
        
        int c = 1;
        for (int i = result.Zeros.Length - 1; i >= 0; i--)
        {
          if (result.Ones[i] > result.Zeros[i])
          {
            gammaRate += c;
          }
          else
          {
            epsilonRate += c;
          }

          c *= 2;
        }

        Console.WriteLine($"Submarine's power consumption is {gammaRate * epsilonRate}"); 
      }
      else
      {
        int? oxygenGeneratorRating = null;
        int? co2ScrubberRating = null;
        
        StringBuilder sbForOxygen = new StringBuilder();
        StringBuilder sbForCO2 = new StringBuilder();
        List<string> remainingBitsForOxygen = new List<string>(bits);
        List<string> remainingBitsForCO2 = new List<string>(bits);
        for (int i = 0; i < bits[0].Length; i++)
        {
          if (!oxygenGeneratorRating.HasValue)
          {
            (int[] Zeros, int[] Ones) resultForOxygen = GetBitsCount(remainingBitsForOxygen);
            sbForOxygen.Append(resultForOxygen.Ones[i] >= resultForOxygen.Zeros[i] ? '1' : '0');
            remainingBitsForOxygen = remainingBitsForOxygen.Where(x => x.StartsWith(sbForOxygen.ToString())).ToList();
            if (remainingBitsForOxygen.Count == 1)
            {
              oxygenGeneratorRating = ConvertBitStringToDecimal(remainingBitsForOxygen[0]);
            }
          }

          if (!co2ScrubberRating.HasValue)
          {
            (int[] Zeros, int[] Ones) resultForCO2 = GetBitsCount(remainingBitsForCO2);
            sbForCO2.Append(resultForCO2.Zeros[i] <= resultForCO2.Ones[i] ? '0' : '1');
            remainingBitsForCO2 = remainingBitsForCO2.Where(x => x.StartsWith(sbForCO2.ToString())).ToList();
            if (remainingBitsForCO2.Count == 1)
            {
              co2ScrubberRating = ConvertBitStringToDecimal(remainingBitsForCO2[0]);
            }  
          }
          
          if (oxygenGeneratorRating.HasValue && co2ScrubberRating.HasValue)
          {
            break;
          }
        }

        Console.WriteLine($"Submarine's life support rating is {oxygenGeneratorRating.GetValueOrDefault() * co2ScrubberRating.GetValueOrDefault()}"); 
      }
    }
    
    private static int ConvertBitStringToDecimal(string value)
    {
      int c = 1;
      int result = 0;
      for (int i = value.Length - 1; i >= 0; i--)
      {
        if (value[i] == '1')
        {
          result += c;
        }

        c *= 2;
      }

      return result;
    }

    private static (int[] Zeros, int[] Ones) GetBitsCount(List<string> bits)
    {
      (int[] Zeros, int[] Ones) result = (null, null);
      foreach (string line in bits)
      {
        if (result.Zeros == null)
        {
          result = (new int[line.Length], new int[line.Length]);
        }
          
        for (int i = 0; i < line.Length; i++)
        {
          if (line[i] == '0')
          {
            result.Zeros[i]++;
          }
          else
          {
            result.Ones[i]++;
          }
        }
      }

      return result;
    }
  }
}