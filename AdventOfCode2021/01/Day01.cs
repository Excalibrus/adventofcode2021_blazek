﻿using System;
using System.IO;

namespace AdventOfCode2021
{
  public class Day01
  {
    public static void Solve(int part)
    {
      int increases = 0;
      int? previousDepth = null;
      int? a = null, b = null, c = null;
      using (StreamReader sr = new StreamReader("01.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          int depth = int.Parse(line);
          if (part == 1)
          {
            if (previousDepth.HasValue && depth > previousDepth.Value)
            {
              increases++;
            }
          
            previousDepth = depth;  
          }
          else
          {
            if (!a.HasValue)
            {
              a = depth;
            }
            else if (!b.HasValue)
            {
              b = depth;
            }
            else if (!c.HasValue)
            {
              c = depth;
            }
            else
            {
              a = b;
              b = c;
              c = depth;
            }
          
            if (b.HasValue && c.HasValue)
            {
              int sum = a.Value + b.Value + c.Value;
              if (sum > previousDepth)
              {
                increases++;
              }
              previousDepth = sum;
            }
          }
        }
      }

      Console.WriteLine($"There are {increases} depth increases");
    }
  }
}