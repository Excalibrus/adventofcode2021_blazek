﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2021
{
  public class Day06
  {
    public static void Solve(int part)
    {
      using StreamReader sr = new StreamReader("06.txt");
      List<int> tmp = sr.ReadLine()?.Split(",").Select(int.Parse).ToList() ?? new List<int>();
      Dictionary<int, double> fishes = new Dictionary<int, double>();
      for (int i = 0; i <= 8; i++)
      {
        fishes.Add(i, 0);
      }
      foreach (int fish in tmp)
      {
        fishes[fish]++;
      }

      for (int day = 1; part == 1 ? day <= 80 : day <= 256; day++)
      {
        double fishesToInstantiate = fishes[0];
        
        for (int i = 0; i < 8; i++)
        {
          fishes[i] = fishes[i + 1];
        }

        fishes[6] += fishesToInstantiate;
        fishes[8] = fishesToInstantiate;
      }

      double sum = 0;
      foreach (KeyValuePair<int,double> pair in fishes)
      {
        sum += pair.Value;
      }

      Console.WriteLine($"After 80 days there is {sum} fishes.");
    }
  }
}