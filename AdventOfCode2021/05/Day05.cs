﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2021
{
  public class Day05
  {
    public static void Solve(int part)
    {
      Dictionary<string, int> fields = new Dictionary<string, int>();
      using StreamReader sr = new StreamReader("05.txt");
      string line;
      while ((line = sr.ReadLine()) != null)
      {
        string[] words = line.Split(" ");
        string[] wordsFrom = words[0].Split(",");
        string[] wordsTo = words[2].Split(",");

        (int X, int Y) from = (int.Parse(wordsFrom[0]), int.Parse(wordsFrom[1]));
        (int X, int Y) to = (int.Parse(wordsTo[0]), int.Parse(wordsTo[1]));

        bool isDiagonal = from.X != to.X && from.Y != to.Y;
        
        if (part == 1 && isDiagonal)
        {
          continue;
        }

        bool isRight = from.X <= to.X;
        bool isDown = from.Y <= to.Y;

        int incrementY = 0;
        for (int x = from.X; isRight ? x <= to.X : x >= to.X; x = isRight ? x + 1 : x - 1)
        {
          for (int y = from.Y + incrementY; isDown ? y <= to.Y : y >= to.Y; y = isDown ? y + 1 : y - 1)
          {
            string key = $"{x}-{y}";
            if (!fields.ContainsKey(key))
            {
              fields.Add(key, 0);
            }

            fields[key]++;

            if (isDiagonal)
            {
              incrementY = isDown ? incrementY + 1 : incrementY - 1;
              break;
            }
          }
        }
      }

      int total = fields.Count(x => x.Value >= 2);
      Console.WriteLine($"Total number of points where at least 2 pipes overlap is: {total}");
    }
  }
}